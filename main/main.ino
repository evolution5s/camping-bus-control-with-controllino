#include <Controllino.h>
/* Usage of CONTROLLINO library allows you to use CONTROLLINO_xx aliases in your sketch. */

// Belegung Camper
// Input Signale
const int T_Flur = CONTROLLINO_A0;     //Taster für Flurbeleuchtung (zwei Taster 1x Sitzecke V2, 1x Bett H1)
const int T_Kueche = CONTROLLINO_A1;    //Taster für Küchenbeleuchtung (zwei Taster 1x Sitzecke V3, 1x Bett H2)
const int T_Bad = CONTROLLINO_A2;       //Taster für Badbeleuchtung (zwei Taster 1x Sitzecke V4, 1x Bett H3)
const int T_H4 = CONTROLLINO_A3;        // Taster hinten 4
const int S_H5 = CONTROLLINO_A4;        // an/aus der Wasserpumpe
const int S_H6 = CONTROLLINO_A5;        // an/aus Lüfter Bad
const int S_H7 = CONTROLLINO_A6;        // an/aus Lüfter unter Bett
const int S_Night = CONTROLLINO_A7;        //H8 Nachtmodus (alle Lampen werden gedimmt und Ventilatoren zurückgefahren
const int S_Main = CONTROLLINO_A8;       // H11 - Hauptschalter zum Abschalten der meisten Funktionen
const int T_Sitzgruppe = CONTROLLINO_A9; //Taster für Sitzgruppenbeleuchtung (V1)
const int Mux_In = CONTROLLINO_A10;  // 4512 ??? Abwasser:(I4-grey-25% I5-white-50% I6-green-75%) Toilette:(I7-yellow-80%) Frischwasser:(I0-grey-20% I1-green-40% I2-brown-60% I3-white-80%)

const int SensCurrentSolar = CONTROLLINO_A11;   // Sensor Stromfluss Solaranlage
const int SensCurrentLiMa =  CONTROLLINO_A12;    // Sensor Stromfluss Lichtmaschine
const int SensCurrentLoading = CONTROLLINO_A13;  // Sensor Stromfluss Ladegerät
const int Temp_Boiler = CONTROLLINO_A14;    //Temperatur am Boiler gemessen mit KTY-81 210 mit 5,1k Spannungsteiler - gemessen zwischen KTY und GND
const int U_Batt = CONTROLLINO_A15;         // Spannnung an der Batterie, Spannungsteiler 2 5,1k Widerstände, Messung zwischen U_Batt und GND
// CONTROLLIN0_I16;
const int T_Funk_A = CONTROLLINO_I17;    //Funktaster A
const int T_Funk_B = CONTROLLINO_I18;    //Funktaster B
const int T_Funk_C = CONTROLLINO_IN0;    //Funktaster C
const int T_Funk_D = CONTROLLINO_IN1;    //Funktaster D


//Output Signale
const int L_Flur = CONTROLLINO_D0; // Licht Flur
const int L_Kueche = CONTROLLINO_D1; // Licht Küche
const int L_Bad = CONTROLLINO_D2; // Licht Bad
const int L_Sitzgruppe = CONTROLLINO_D3; // Licht Sitzgruppe
const int L_Fahrerhaus = CONTROLLINO_D4; // Licht Fahrerhaus
const int V_Bad = CONTROLLINO_D5; // Ventilator Bad
const int V_Toilett = CONTROLLINO_D6; // Ventilator Toilett
const int V_Stau = CONTROLLINO_D7;       // Ventilator Stauraum

const int PWM_Boiler = CONTROLLINO_D10; // PWM Ausgang für Phasenanschnittssteuerung Boiler (10-90%) (benutzt Timer 1)
const int LED_Toilett = CONTROLLINO_D12; // Warn-LED Toilett
const int Power_Sens = CONTROLLINO_D13;    // Stromversorgung für Sensoren
const int Piepser = CONTROLLINO_D16;
const int MUX4512_S0 =  CONTROLLINO_D17;    // Switch für Multiplexer Max328
const int MUX4512_S1 =  CONTROLLINO_D18;    // Switch für Multiplexer Max328
const int MUX4512_S2 =  CONTROLLINO_D19;    // Switch für Multiplexer Max328

//Relais-Output Signale
const int Trennrelais = CONTROLLINO_R10;    //Masseschaltung für Trennrelais zum Ab- und Anschalten desselbigen
const int Pumpe = CONTROLLINO_R12;  //Wasserpumpe
const int Pump_Toilett = CONTROLLINO_R13;  // Pumpe Toilett

//Relais Output 230V

const int Steckdose_Fahrerh = CONTROLLINO_R1;
const int Steckdosen_vorne = CONTROLLINO_R2;
const int Steckdosen_hinten = CONTROLLINO_R3;
const int Freezer = CONTROLLINO_R4;
const int Dishwasher = CONTROLLINO_R5;
const int Kochfeld1 = CONTROLLINO_R6; // Relais Kochfeld, Achtung, nur zusammen mit Kochfeld2 schalten!!!
const int Kochfeld2 = CONTROLLINO_R7;
const int Boiler1 = CONTROLLINO_R8;
const int Boiler2 = CONTROLLINO_R9;

#define PIN_Var 100    // Anzahl von benötigten PIN-Variablen
int state[PIN_Var];			// Defines the analog or digital state of the outputpin
int dim[PIN_Var];			// Defines the last direction of the dim function down=0, up=1
int flag[PIN_Var];
int previous[PIN_Var];    // the previous reading from the input pin
// the follow variables are long's because the time, measured in miliseconds,
// will quickly become a bigger number than can be stored in an int.
long time1[PIN_Var];       // the last time the output pin was toggled

#define count_u_float 250  // Spannungsmessung für Warmwasserheizung über 250x5 Sekunden
#define count_u_float_low 1200  // Spannungsmessung für Low Voltag Detection über 1200x5 Sekunden = 2 Stunden


int i_float;              // globale Zählvariable zu Float Modus für Warmwasserheizung
int i_float_low;              // globale Zählvariable zu Float Modus für Low Voltage Detection

float u_float[count_u_float];         // array zur Speicherung  um zu ermitteln ob Float-Modus vorliegt für Warmwasserheizung
float u_float_low[count_u_float_low];         // array zur Speicherung um low Voltage über eine Stunde zu erkennen.

float u_ges =14.0;                   // Gesamtspannung über den Mittelwert von x Spannungswerten für Warmwasserheizung
float u_ges_low =13.0;                   // Gesamtspannung über den Mittelwert von x Spannungswerten für Low Voltage Detection


const int debounce = 200;   // the debounce time, increase if the output flickers
const int nightlight = 1;   // Helligkeitslevel auf welches die Tasten im Nachtmodus geschaltet werden
const int dim_speed = 25;     // alle x ms wird die Funktion dimmen beim dimmen wieder aufgerufen, so wird die Dimmgeschwindigkeit bestimmt, je kürzer desto schneller
long time_sensors;
long tmp_Ubatt = 0;
int i_Ubatt = 0;
int  i_pwm=0; // PWM-Wert für die Wassererwärmung

void setup()
{
  time_sensors = 0;       // Zeitvariable für Sensorabfrage der Funktion query_Sensors
  for (int i = 0; i < PIN_Var; i++) {
    previous[i] = HIGH;
    time1[i] = 0;
    state[i] = 0;
    dim[i] = 1;
    flag[i] = 0;
  }
  for (int i = 0; i < count_u_float; i++) {
    u_float[i]=13.5;
  }
  i_float=0;
  Serial.begin(9600);   //Serielle Datenverbindung für Kommunikation mit dem PC
  Serial.println("Test");
  
  pinMode(CONTROLLINO_D0, OUTPUT);
  pinMode(CONTROLLINO_D1, OUTPUT);
  pinMode(CONTROLLINO_D2, OUTPUT);
  pinMode(CONTROLLINO_D3, OUTPUT);
  pinMode(CONTROLLINO_D4, OUTPUT);
  pinMode(CONTROLLINO_D5, OUTPUT);
  pinMode(CONTROLLINO_D6, OUTPUT);
  pinMode(CONTROLLINO_D7, OUTPUT);
  pinMode(CONTROLLINO_D8, OUTPUT);
  pinMode(CONTROLLINO_D9, OUTPUT);
  pinMode(CONTROLLINO_D10, OUTPUT);
  pinMode(CONTROLLINO_D11, OUTPUT);
  pinMode(CONTROLLINO_D12, OUTPUT);
  pinMode(CONTROLLINO_D13, OUTPUT);
  pinMode(CONTROLLINO_D16, OUTPUT);
  pinMode(CONTROLLINO_D17, OUTPUT);
  pinMode(CONTROLLINO_D18, OUTPUT);
  pinMode(CONTROLLINO_D19, OUTPUT);

  pinMode(CONTROLLINO_R0, OUTPUT);
  pinMode(CONTROLLINO_R1, OUTPUT);
  pinMode(CONTROLLINO_R2, OUTPUT);
  pinMode(CONTROLLINO_R3, OUTPUT);
  pinMode(CONTROLLINO_R4, OUTPUT);
  pinMode(CONTROLLINO_R5, OUTPUT);
  pinMode(CONTROLLINO_R6, OUTPUT);
  pinMode(CONTROLLINO_R7, OUTPUT);
  pinMode(CONTROLLINO_R8, OUTPUT);
  pinMode(CONTROLLINO_R9, OUTPUT);
  pinMode(CONTROLLINO_R10, OUTPUT);
  pinMode(CONTROLLINO_R11, OUTPUT);
  pinMode(CONTROLLINO_R12, OUTPUT);
  pinMode(CONTROLLINO_R13, OUTPUT);

  pinMode(CONTROLLINO_A0, INPUT);
  pinMode(CONTROLLINO_A1, INPUT);
  pinMode(CONTROLLINO_A2, INPUT);
  pinMode(CONTROLLINO_A3, INPUT);
  pinMode(CONTROLLINO_A4, INPUT);
  pinMode(CONTROLLINO_A5, INPUT);
  pinMode(CONTROLLINO_A6, INPUT);
  pinMode(CONTROLLINO_A7, INPUT);
  pinMode(CONTROLLINO_A8, INPUT);
  pinMode(CONTROLLINO_A9, INPUT);
  pinMode(CONTROLLINO_A10, INPUT);
  pinMode(CONTROLLINO_A11, INPUT);
  pinMode(CONTROLLINO_A12, INPUT);
  pinMode(CONTROLLINO_A13, INPUT);
  pinMode(CONTROLLINO_A14, INPUT);
  pinMode(CONTROLLINO_A15, INPUT);

  pinMode(CONTROLLINO_I17, INPUT);
  pinMode(CONTROLLINO_I18, INPUT);
  pinMode(CONTROLLINO_IN0, INPUT);
  pinMode(CONTROLLINO_IN1, INPUT);

  
  // Test Piepser
  int t_pieps = 20;
  // for(int i=0;i<5;i++){
  //    digitalWrite(Piepser,HIGH);
  //    delay(t_pieps);
  //    digitalWrite(Piepser,LOW);
  //    delay(t_pieps*10);
  //  }
  //Test End
}
// ********************************************** main loop **********************************************

void loop()
{

  digitalWrite(Boiler1, HIGH); //Test
  digitalWrite(Boiler2, HIGH);
  
  pushbutton(T_Flur, L_Flur); //Licht Flur
  pushbutton(T_Kueche, L_Kueche); //Licht Küche
  pushbutton(T_Bad, L_Bad); //Licht Bad
  pushbutton(T_Sitzgruppe, L_Sitzgruppe); //Licht Sitzgruppe
  switchc(S_H5, Pumpe, HIGH, LOW, 255); // Pumpe on/off
  switchc(S_H6, V_Bad, HIGH, HIGH, 255); // Lüfter Bad on/off bzw gedimmt in der Nacht
  switchc(S_H7, V_Stau, HIGH, HIGH, 255); // Lüfter Stauraum on/off bzw gedimmt in der Nacht

  funcS_Main(5);   // Abschalten der definierten Funktionen - Spülmaschine und Boiler werden erst nach 5 Stunden abgeschaltet
  if (digitalRead(S_Night) == HIGH) state[S_Night] = nightlight;  //Nachtlichtfunktion aktiviert
  else state[S_Night] = 255;


  fernb(T_Funk_A, Pump_Toilett, 2000, 300000); // Pumpe Toilett wird durch 2 Sekunden drücken von Taste A betätigt und läuft 5 Minuten
  fernb(T_Funk_C, Trennrelais, 1000, 21600000); // Trennrelais wird durch 1 Sekunden drücken von Taste C betätigt und läuft 6 Stunden durch einen weiteren Tastendruck wird es deaktiviert

  if (digitalRead(T_Funk_D) == HIGH){  // mit Taste D der Fernbedienung alle Lichter ausschalten
    analogWrite(L_Flur, 0);
    analogWrite(L_Kueche, 0);
    analogWrite(L_Bad, 0);
    analogWrite(L_Sitzgruppe, 0);
  }

 digitalWrite(Freezer, HIGH);

 query_sensors(5000); // Sensorabfrage alle 5 Sekunden

}
//*********************************************** end main loop **********************************************

void pushbutton(int Taster, int LED) {	//Pushbutton wird ausgelesen und LED oder Lame entsprechend eingeschaltet und bei längerem Drücken gedimmt
  int reading = digitalRead(Taster);
  // if the input just went from LOW and HIGH and we've waited long enough
  // to ignore any noise on the circuit, toggle the output pin and remember
  // the time
  if (reading == HIGH && previous[Taster] == LOW) {
    flag[Taster] = 1;
    time1[Taster] = millis();
    if (dim[Taster] == 0) dim[Taster] = 1;
    else dim[Taster] = 0;
  }

  if (reading == LOW && flag[Taster] == 1 && millis() - time1[Taster] > debounce) {
    flag[Taster] = 0;
    if (state[Taster] > 0) state[Taster] = 0;
    else	state[Taster] = state[S_Night];
    time1[Taster] = millis();
    analogWrite(LED, state[Taster]);
  }

  if (reading == HIGH && previous[Taster] == HIGH && millis() - time1[Taster] >= 400 && millis() - time1[Taster] >= 400 + dim_speed) {
    flag[Taster] = 0;
    if (state[Taster] == 0) dim[Taster] = 1;
    if (state[Taster] == 255) dim[Taster] = 0;
    if (state[Taster] >= 0 && state[Taster] <= 255) {
      if (dim[Taster] == 1) state[Taster] += 1;
      if (dim[Taster] == 0) state[Taster] -= 1;
    }
    time1[Taster] = millis() - 401; // Setze time auf einen Wert 401 ms kleiner als millis um wieder in die Funktion zu laufen, aber einen Zeitreferenzwert für das Dimmen zu erhlaten
    analogWrite(LED, state[Taster]);
  }
  previous[Taster] = reading;
}

void switchc(int Switch, int Funktion, int incMain, int incNight, int valueNight) { //incMain und incNight betrachten den Hauptschalter und den Nachtmodus ob die Funktion ausgeschaltet werden soll wenn aktiv - HIGH == ja, LOW == nein
  //Value Night gibt die Höhe des Dimmwerts für die nacht vor
  int reading = digitalRead(Switch);
  int i = 0;
  if (reading == HIGH && (digitalRead(S_Main) == HIGH || incMain == LOW)) {
    i = 255;
    if (digitalRead(S_Night) == HIGH || incNight == LOW) i = valueNight;
  }
  else i = 0;
  analogWrite( Funktion, i);
}

void fernb(int Taster, int Function, long delay_time, long off_time) { // Funktion wird geschaltet wenn Taster mindestens
  //delay_time in Sekunden gedrückt wird, Ausschalten nach off_time in ms, Ausschalten auch durch Tastendruck nach off_time+2s möglich
  int reading = digitalRead(Taster);
  if (reading == LOW && flag[Taster] == 0) time1[Taster] = millis();
  if (millis() - time1[Taster] >= delay_time) {
    if (flag[Taster] == 0)   funcPiepser(20,200,3);
    flag[Taster] = 1;
    state[Taster] = HIGH;
    }
  if (millis() - time1[Taster] >= off_time || (millis() - time1[Taster] > delay_time + 2000  && reading == HIGH)) { //Abschalten nach off_time oder wenn ein erneuter Ausschaltimpuls mindestens 2 Sekunden nach dem Einschalten kommt
    state[Taster] = LOW;
    flag[Taster] = 0;
    funcPiepser(40,400,1);
    }
    digitalWrite(Function, state[Taster]);
}

void funcS_Main(int off_time) { //off Time in Stunden
  int reading = digitalRead(S_Main);
  if (reading == HIGH ) {
    flag[S_Main] = 0;
    //TCCR4B = TCCR4B & 0b11111000 | 0x05;  // Test xxx auskommentiert da nicht verwendet (3 Zeilen + zeilen in else)
    analogWrite(V_Toilett, 255); // Lüfter in Toilett im Nachtmodus oder wenn der Hauptschalter aus ist auf halbe Leistung regeln
    //TCCR4B = TCCR4B & 0b11111000 | 0x03;
    digitalWrite(Dishwasher, HIGH);
    digitalWrite(Steckdose_Fahrerh, HIGH);
    digitalWrite(Steckdosen_vorne, HIGH);
    digitalWrite(Steckdosen_hinten, HIGH);
    digitalWrite(Kochfeld1, HIGH);
    digitalWrite(Kochfeld2, HIGH);
    digitalWrite(Dishwasher, HIGH);

  }
  else {
    //TCCR4B = TCCR4B & 0b11111000 | 0x05;
    analogWrite(V_Toilett, 255); // Lüfter in Toilett im Nachtmodus oder wenn der Hauptschalter aus ist auf halbe Leistung regeln
    //TCCR4B = TCCR4B & 0b11111000 | 0x03;
    digitalWrite(Steckdose_Fahrerh, LOW);
    digitalWrite(Kochfeld1, LOW);
    digitalWrite(Kochfeld2, LOW);
  }
  if (reading == LOW && previous[S_Main] == HIGH && flag[S_Main] == 0) {
    time1[S_Main] = millis();
    flag[S_Main] = 1;
  }
  if (reading == LOW && millis() - time1[S_Main] >= off_time * 3600000 ) { //Abschalten nach off_time
    digitalWrite(Dishwasher, LOW);
    digitalWrite(Steckdosen_vorne, LOW);
    digitalWrite(Steckdosen_hinten, LOW);
    flag[S_Main] = 0;
  }
}

void query_sensors(int delayS) {
  if (millis() - (time_sensors - 500) > delayS) digitalWrite(Power_Sens, HIGH); // Power für Sensoren an
  if (millis() - time_sensors > delayS) {

//      analogReference(INTERNAL2V56);
//      tmp_Ubatt+=analogRead(U_Batt);
//      i_Ubatt++;
//      analogReference(DEFAULT);
//
//      if (i_Ubatt == 100){
//        i_Ubatt =0;
//        Serial.print("Spannung Batterie: ");
//        Serial.println(tmp_Ubatt/100);
//        tmp_Ubatt =0;
//        }
      
    
    Serial.print("Temp Boiler: ");
    int i = analogRead(Temp_Boiler);
    Serial.println(i);
    Serial.print("U_Batt: ");
    float u=0;
    for (int i=0; i<=9; i++){
    u += analogRead(U_Batt)*2.015*0.015;
    }
    Serial.println(u/10);
    multiplex(HIGH,HIGH,HIGH);
    
    multiplex(LOW,LOW,LOW);
    delay(1);
    i = analogRead(Mux_In);
    Serial.print("I0: ");
    Serial.println(i);
    multiplex(LOW,LOW,HIGH);
    delay(1);
    i = analogRead(Mux_In);
    Serial.print("I1: ");
    Serial.println(i);
    multiplex(LOW,HIGH,LOW);
    delay(1);
    i = analogRead(Mux_In);
    Serial.print("I2: ");
    Serial.println(i);
    multiplex(LOW,HIGH,HIGH);
    delay(1);
    i = analogRead(Mux_In);
    Serial.print("I3: ");
    Serial.println(i);
    multiplex(HIGH,LOW,LOW);
    delay(1);
    i = analogRead(Mux_In);
    Serial.print("I4: ");
    Serial.println(i);
    multiplex(HIGH,LOW,HIGH);
    delay(1);
    i = analogRead(Mux_In);
    Serial.print("I5: ");
    Serial.println(i);
    multiplex(HIGH,HIGH,LOW);
    delay(1);
    i = analogRead(Mux_In);
    Serial.print("I6: ");
    Serial.println(i);
    multiplex(HIGH,HIGH,HIGH);
    delay(1);
    i = analogRead(Mux_In);
    Serial.print("I7: ");
    Serial.println(i);


    funcBoilerControl();
    funcWasserauswertung();    
    
    time_sensors = millis();
  }

}

void multiplex(int i, int j, int k){
  digitalWrite(MUX4512_S0,k);
  digitalWrite(MUX4512_S1,j);
  digitalWrite(MUX4512_S2,i);
}

void funcPiepser (int on_time, int off_time, int repetitions){
  for(int i=0;i<repetitions;i++){
          digitalWrite(Piepser,HIGH);
          delay(on_time);
          digitalWrite(Piepser,LOW);
          delay(off_time);
        }
}

void funcBoilerControl(){  //automatische Regelung der Wassererhitzung
  int i_pwm_min = 30;
  int i_pwm_max_bulk = 60;
  int i_pwm_max_float = 80;
  
  float u = 0; // Batteriespannung
  for (int i=0; i<=9; i++){
    u += analogRead(U_Batt)*2.015*0.015;
    }
  u=u/10;
  float u_heat=14.2;  // Regelung Bulk und Absorbtion Betrieb
  if(u>u_heat){
    i_pwm = i_pwm+(u-u_heat)*10;
    if (i_pwm<i_pwm_min) i_pwm=i_pwm_min;
    if(i_pwm>i_pwm_max_bulk) i_pwm=i_pwm_max_bulk;
  }
  // Ende Regelung Bulk und Absorbtion Betrieb
  
  //Regelung Float Betrieb
  u_ges = u_ges + u/count_u_float - u_float[i_float]/count_u_float;  // fortlaufende Erneuerung der Gesamtspannung 
  u_float[i_float]=u;
  i_float++;
  if (i_float >=count_u_float) i_float=0;
  
  if (u>13.6 && u<13.9 && u_ges > 13.6){
    i_pwm = i_pwm + 3;
    if (i_pwm<i_pwm_min) i_pwm=i_pwm_min;
    if(i_pwm>i_pwm_max_float) i_pwm=i_pwm_max_float;
    } // Regelung Float-Betrieb Ende
  else { 
    if (u<=u_heat){  // Reduzierungsbedingung für Bulk, Absorbtion und Float Modus
      int deltaU = (u_heat - u)*10;
      if (deltaU < 0) deltaU = 0;
      if (deltaU > 5) deltaU = 5;
      i_pwm = i_pwm - deltaU;
      if(i_pwm<0) i_pwm=0;
      } // Reduzierungsbedingung Ende
    }
  analogWrite(PWM_Boiler, i_pwm);
  Serial.print("PWM_Boiler: ");
  Serial.println(i_pwm);
  Serial.print("U_Batt: ");
  Serial.println(u);
  Serial.print("u_ges: ");
  Serial.println(u_ges);
}

void funcWasserauswertung(){
  multiplex(LOW,LOW,LOW);
    delay(1);
    int I0 = analogRead(Mux_In);
     multiplex(LOW,LOW,HIGH);
    delay(1);
    int I1 = analogRead(Mux_In);
    multiplex(LOW,HIGH,LOW);
    delay(1);
    int I2 = analogRead(Mux_In);
    multiplex(LOW,HIGH,HIGH);
    delay(1);
    int I3 = analogRead(Mux_In);  
    multiplex(HIGH,LOW,LOW);
    delay(1);
    int I4 = analogRead(Mux_In);
    multiplex(HIGH,LOW,HIGH);
    delay(1);
    int I5 = analogRead(Mux_In);
    multiplex(HIGH,HIGH,LOW);
    delay(1);
    int I6 = analogRead(Mux_In);
    multiplex(HIGH,HIGH,HIGH);
    delay(1);
    int I7 = analogRead(Mux_In);

    if (I3<500 )Serial.println("Füllstand Frischwasser 80-100%");
    if (I3>500 && I2<500)  Serial.println("Füllstand Frischwasser 60-80%");
    if (I2>500 && I1<500)  Serial.println("Füllstand Frischwasser 40-60%");
    if (I1>500 && I0<500)  Serial.println("Füllstand Frischwasser 20-40%");
    if (I0>500)  Serial.println("Füllstand Frischwasser 0-20%");

    if (I4>500 )Serial.println("Füllstand Abwasser  0-25%");
    if (I4<500 && I5>500)Serial.println("Füllstand Abwasser  25-50%");
    if (I5<500 && I6>500)Serial.println("Füllstand Abwasser  50-75%");
    if (I6<500)Serial.println("Füllstand Abwasser  75-100%");

    float u = 0;
    for (int i=0; i<=9; i++){
      u += analogRead(U_Batt)*2.015*0.015;
      }
    u=u/10;
    float i = analogRead(SensCurrentSolar)*0.015;
    Serial.print("Stromfluss Solaranlage: ");
    Serial.println(i);
    Serial.print("Stromfluss Solaranlage (Ampere): ");
    Serial.println((u/2-i)/0.0334);
    i = analogRead(SensCurrentLiMa)*0.015;
    Serial.print("Stromfluss Lichtmaschine: ");
    Serial.println(i);
    Serial.print("Stromfluss Lichtmaschine (Ampere): ");
    Serial.println((u/2-i)/0.0334);
    i = analogRead(SensCurrentLoading)*0.015;
    Serial.print("Stromfluss Ladegerät: ");
    Serial.println(i);
    Serial.print("Stromfluss Ladegerät (Ampere): ");
    Serial.println((u/2-i)/0.0334);


    if (I7>500){
      Serial.println("Füllstand Urin weniger als 75%");
      digitalWrite(LED_Toilett, LOW);
    }
    else{
      Serial.println("Füllstand Urin mehr als 75% !!!");
       digitalWrite(LED_Toilett, HIGH);
    }
}

void lowVoltageDetection(){
  float u = 0; // Batteriespannung
  for (int i=0; i<=9; i++){
    u += analogRead(U_Batt)*2.015*0.015;
    }
  u=u/10;
  
  u_ges_low = u_ges_low + u/count_u_float_low - u_float_low[i_float_low]/count_u_float_low;  // fortlaufende Erneuerung der Gesamtspannung 
  u_float_low[i_float_low]=u;
  i_float_low++;
  if (i_float_low >=count_u_float_low) i_float_low=0;
  
  Serial.print("U_ges für Low Voltage Detection: ");
  Serial.println(u_ges_low);
}

